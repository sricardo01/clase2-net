﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital
{
    class Empleado: Persona
    {
        
        // Atributos
        
        private String especialidad;
        private Double sueldo; // 500 , 505.3546
        private int mesesTrabajados; // 500, 600 


        // Metodos

        public double calcularSueldoPorMesesTrabajados()
        {
            // Valida datos
            if(mesesTrabajados < 1)
            {
                return 0;
            }

            if(sueldo < 0)
            {
                return 0;
            }
            return mesesTrabajados * sueldo;
        }

        // virtual --> Necesario para aplicar sobreescritura
        public virtual String nombres()
        {
            return Nombre + " "+  Apellido;
        }

        // 0
        public double calcularSueldoPorMesesTrabajados(double porcentajeDescuento)
        {
            return calcularSueldoPorMesesTrabajados()  * porcentajeDescuento; 
        }


        public void ingresarEmpleado(string path)
        {
            UtilArchivo util = new UtilArchivo();
            UtilFecha utilFecha = new UtilFecha();
            if(util.comprabarQueExistaArchivo(path))
            {
                string linea;
                StreamReader lectura = new StreamReader(path);
                while ((linea = lectura.ReadLine()) != null)
                {

                    string[] datosCsv = linea.Split(',');
                    string fecha = datosCsv[0];
                    string nombre = datosCsv[1];
                    string apellido = datosCsv[2];
                    Empleado empleado = new Empleado();
                    empleado.Nombre = nombre;
                    empleado.Apellido = apellido;
                    empleado.FechaIngreso = utilFecha.tranformarFechaStringAdateTime(fecha);
                    Console.WriteLine("Se a ingresa correctamente el empleado: " + empleado.nombres());

                }
            }

        }


        public virtual  String hola() {
            return "HOLA";
        }

        // 
        public override string ToString()
        {
            return Nombre + " " + Apellido;
        }

        // Gets ands Sets

        public Double Sueldo
        {
            get { return sueldo; }
            set { sueldo = value; }
        }

        public String Especialidad
        {
            get { return especialidad; }
            set { especialidad = value; }
        }


        public int MesesTrabajados
        {
            get { return mesesTrabajados; }
            set { mesesTrabajados = value; }
        }



    }
}
