﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital
{
    class Persona
    {

        // Atributos
        private String apellido;
        private String nombre;
        private DateTime fechaNacimiento;
        private DateTime fechaIngreso;
        

        // Metodos, siempre en verbo
        public int calcularEdad(int anio)
        {
            // 2020 - 1993  = 27
            int anioFechaNacimiento = fechaNacimiento.Year;
            return anio - anioFechaNacimiento;
        }


        public string obtenerDatosEmpleado(String especialidad)
        {
            return Nombre + " tiene una especialidad en " + especialidad;
        }

       /* public string obtenerDatosEmpleado(String especialidad, int sueldo)
        {
            return Nombre + "tiene una especialidad en " + especialidad + " y actualmente tiene" +
                "un sueldo de " + sueldo;
        }*/

        public string obtenerDatosEmpleado(String especialidad, double sueldo)
        {
            return Nombre + "tiene una especialidad en " + especialidad + " y actualmente tiene" +
                "un sueldo de " + sueldo;
        }





        // Metodos Get and Set

        public String Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public DateTime FechaNacimiento
        {
            get { return fechaNacimiento; }
            set { fechaNacimiento = value; }
        }

        public DateTime FechaIngreso
        {
            get { return fechaIngreso; }
            set { fechaIngreso = value; }
        }


    }
}
