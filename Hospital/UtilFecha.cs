﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital
{
    class UtilFecha
    {

        // Stalyn 
        public DateTime tranformarFechaStringAdateTime(string fecha)
        {
            string[] formatos = new string[] { "yyyy-MM-dd", "yyyy/MM/dd", "dd/MM/yyyy" };
            return DateTime.ParseExact(fecha, formatos, CultureInfo.InvariantCulture, DateTimeStyles.None);
        }

        // Sobrecarga
        // Ricardo
        public DateTime tranformarFechaStringAdateTime(string fecha, string formato)
        {
            return DateTime.ParseExact(fecha, formato, CultureInfo.InvariantCulture, DateTimeStyles.None);
        }


    }
}
