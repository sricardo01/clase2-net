﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital
{
    class UtilArchivo
    {
       
        public Boolean comprabarQueExistaArchivo(string path)
        {
            if(File.Exists(path))
            {
                return true;
            }

            return false;
        }

        public int numeroDeRegistro(string path)
        {
            int contador = 0;
            string linea;
            if (comprabarQueExistaArchivo(path))
            {
                StreamReader lectura = new StreamReader(path);
                while ((linea = lectura.ReadLine()) != null)
                {
                    contador++;
                }
            }
            return contador;
        }

        public void mostrarContenidoArchivo(string path)
        {
            string linea;
            if (comprabarQueExistaArchivo(path))
            {
                StreamReader lectura = new StreamReader(path);
                while ((linea = lectura.ReadLine()) != null)
                {
                    // string[] datos = linea.Split(',');
                    Console.WriteLine(linea);
                   // Console.WriteLine("POS 0 : "+ datos[0] +" POS 1: "  + datos[1] + " POS 2: " + datos[2]);
                }
            }
        }



    }
}
